#!/usr/bin/env groovy

def getImageTag(String branch) {
  if (branch == 'master' || branch == 'staging') {
    return branch
  }
  return 'sandbox'
}