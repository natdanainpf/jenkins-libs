#!groovy

def call() {
    emailext (
        subject: "Jenkins - ${env.JOB_NAME} Job " + currentBuild.currentResult,
        to: 'team@firecreekweb.com',
        from: 'technical@firecreekweb.com',
        mimeType: 'text/html',
        body: '''${SCRIPT, template="email-html-custom.template"}'''
    )
}