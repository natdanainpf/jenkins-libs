#!/usr/bin/env groovy

def call(String format = '') {
     def parentBranches = sh(script: "git branch -a --list origin/master origin/staging origin/hotfix || exit 1", returnStdout: true).trim()
     if (parentBranches == false) {
         parentBranches = ''
     }
     def ancestorCommit = sh(script: "git merge-base HEAD ${parentBranches} || exit 1", returnStdout: true).trim()
     def lastCommit = sh(script: 'git log -1 --pretty=%H', returnStdout: true).trim()
     echo "==> Common ancestor is ${ancestorCommit}, last commit is ${lastCommit}."
     if (lastCommit.equals(ancestorCommit)) {
         commitHashes = [sh(script: "git log -1 --pretty=%H", returnStdout: true).trim()]
     } else {
         commitHashes = sh(script: "git rev-list -5 ${ancestorCommit}..", returnStdout: true).trim().tokenize('\n')
     }

    def message = []
    def openParagraph = ''
    def closeParagraph = ''
    def markDown = ''

    if (format == 'html') {
        openParagraph = '<p>'
        closeParagraph = '</p>'
        markDown = ''
    } else if(format == 'slack') {
        openParagraph = ''
        closeParagraph = '\n'
        markDown = '```'
    }

    for (commit in commitHashes) {
        def author = sh(script: "git log -1 --pretty=%an ${commit}", returnStdout: true).trim()
        def commitMsg = sh(script: "git log -1 --pretty=%B ${commit}", returnStdout: true).trim()
        def shortCommitId = sh(script: "git rev-parse --short ${commit}", returnStdout: true).trim()
        if (format == 'html') {
            message.add("${openParagraph} ${shortCommitId} by ${author} ${closeParagraph}")
        } else if (format == 'slack') {
            message.add("${markDown} ${commitMsg} ${markDown} ${closeParagraph}")
        }
    }
    return message
}