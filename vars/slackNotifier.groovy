#!/usr/bin/env groovy

def call(String buildResult, String slackChannel = 'feed-jenkins') {
  if ( buildResult == "STARTED" ) {
    slackSend channel: slackChannel, color: "good", message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Started (<${env.BUILD_URL}|Open>)"
  }
  else if ( buildResult == "SUCCESS" ) {
    slackSend channel: slackChannel, color: "good", message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Success (<${env.BUILD_URL}|Open>)"
  }
  else if( buildResult == "FAILURE" ) { 
    slackSend channel: slackChannel, color: "danger", message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Failed (<${env.BUILD_URL}|Open>)"
  }
  else if( buildResult == "UNSTABLE" ) { 
    slackSend channel: slackChannel, color: "warning", message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} Unstable"
  }
  else {
    slackSend channel: slackChannel, color: "danger", message: "${env.JOB_NAME} - #${env.BUILD_NUMBER} its result was unclear"
  }
}