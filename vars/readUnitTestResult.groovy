#!/usr/bin/env groovy

def call() {
    def matcher = readFile('junit.xml') =~ '<testsuite name="(.*)" tests="(.*)" assertions="(.*)" failures="(.*)" errors="(.*)" time="(.*)">'
    def message = ''
    if (matcher) {
        message = """
PHPUnit Test Result
 Name: ${matcher[0][1]}  Test: ${matcher[0][2]}
 Assertions: ${matcher[0][3]}  Failures: ${matcher[0][4]}
 Errors: ${matcher[0][5]}  Time: ${matcher[0][6]}
<${env.BUILD_URL}/testReport|Click here to see PHPUnit test report>
"""
    }
    return message
}