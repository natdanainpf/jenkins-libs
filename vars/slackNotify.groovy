#!/usr/bin/env groovy

def call(String buildStatus, String additionalMsg = '') {
    buildStatus = buildStatus ?: 'Success'
    // Red default
    def colorCode = '#A30200'
    def details = """
*${buildStatus}* : Job ${env.JOB_NAME}
Stage : ${env.STAGE_NAME}
<${env.BUILD_URL}|Build no. #${env.BUILD_NUMBER}>
${additionalMsg}
"""
    if (buildStatus == 'Started') {
        // Blue
        colorCode = '2684FF'
    } else if (buildStatus == 'Success') {
        // Green
        colorCode = '#2EB886'
    } else if (buildStatus == 'Unstable') {
        // Orange
        colorCode = '#DAA038'
    } else if (buildStatus == 'Aborted') {
        // Red
        colorCode = '#A30200'
    }

    // Send notifications
    slackSend (channel: env.SLACK_CHANNEL, color: colorCode, message: details)
}