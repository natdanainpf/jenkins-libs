#!/usr/bin/env groovy

def call(String projectDir, String url, String accessKey, String secretKey, String stack) {
  dir("${projectDir}") {
    sh "rancher-compose --url ${url} --access-key ${accessKey} --secret-key ${secretKey} -p ${stack} --verbose up -d --force-upgrade --pull --confirm-upgrade"
  }
}